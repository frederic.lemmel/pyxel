.. _notebook_api:

========
Notebook
========

General
=======

.. currentmodule:: pyxel

.. autofunction:: display_detector

.. autofunction:: display_html


Displaying calibration inputs and outputs in notebooks
======================================================

.. autofunction:: display_calibration_inputs

.. autofunction:: display_simulated

.. autofunction:: display_evolution

.. autofunction:: optimal_parameters

.. autofunction:: champion_heatmap
